# bivariate_sample

## Setup

- [Install Gramex 1.x](https://learn.gramener.com/guide/install/)
- Clone this repository
- Copy assets from shared repo, e.g. `demo.gramener.com:/deploy/<user>/<repo>/`
- From the repo folder, run `gramex setup .`
- From the repo folder, run `gramex`

## install notes
- `cd /mnt/rradrive/gramex_install/`
- `source /home/ubuntu/anaconda3/bin/virtualenvwrapper.sh`
- `workon gramex`
- removal: `deactivate` `rmvirtualenv gramex`


## start docker instance 
- `cd /mnt/rradrive/apps/gramex_ifri_commfor`
- start: `sudo nohup docker run --name gramex-ifricommfor -p 9988:9988 gramener/gramex run &`
- reconnect: `docker start -i -a gramex-ifricommfor`
- before restart: `docker rm gramex-ifricommfor`
## Contributions

- Author <user@example.org>
