async function vega_render(conf){
  var script = document.currentScript
  var id = conf.id || String(Math.random()) || script.getAttribute('data-id')
  var loading = conf.loading || '<h3>Loading...</h3><i class="fa fa-spin fa-spinner fa-2x"></i>'
  var renderer = conf.renderer || 'svg'
  var spec = conf.spec || {}
  var width = spec.width || 400 || +script.getAttribute('data-width')
  var height = spec.height || 200 || +script.getAttribute('data-height')
  var padd = {"left": 5, "top": 15, "right": 5, "bottom": 5}

  spec.width = width
  spec.height = height
  spec.padding = padd;
  var vg_opt = {"actions": {export: true, source: false, compiled: false, editor: false},
                "downloadFileName": _.isUndefined(conf["spec"]["title"] == false) ? conf["spec"]["title"]["text"] : ""}
  var container = document.getElementById(id);
  vegaEmbed(`#${id}`, spec, vg_opt)
    // result.view provides access to the Vega View API
    .then(result => {
      console.log(["rendered vega-chart ", id]);
      $(".mark-text text").css("opacity", 1); 
      $("body").removeClass("loading");
      container.vega = result.view;
    }).catch(err => {
      console.warn(["Err rendering chart-id ", id]);
      $("body").removeClass("loading");
    });
}

async function vega_render_old(conf) {
  var script = document.currentScript
  var id = conf.id || String(Math.random()) || script.getAttribute('data-id')
  var loading = conf.loading || '<h3>Loading...</h3><i class="fa fa-spin fa-spinner fa-2x"></i>'
  var renderer = conf.renderer || 'svg'
  var spec = conf.spec || {}
  var width = spec.width || 400 || +script.getAttribute('data-width')
  var height = spec.height || 200 || +script.getAttribute('data-height')
  var padd = {"left": 5, "top": 20, "right": 5, "bottom": 5}

  //document.write('<div id="' + id + '_chart" style="text-align:center;width:' + width + 'px;height:' + height + 'px">' + loading + '</div>')
  //document.addEventListener('DOMContentLoaded', function () {
    var container = document.getElementById(id);
    $(container).append(loading);
    try {
      container.spec = {}
      if ('fromjson' in spec) {
        container.spec.vegam = spec.fromjson
        spec = vegam.vegam([]).fromjson(spec.fromjson).spec
      }
      spec.width = width
      spec.height = height
      spec.padding = padd;
      if (spec['$schema'].endsWith('vega-lite/v2.json')) {
        container.spec.vegalite = spec
        spec = vl.compile(spec).spec
      }
      container.spec.vega = spec
      var view = await new vega.View(vega.parse(spec))
        .renderer(renderer)
        .initialize(container)
        .hover()
        .run()
      container.vega = view
    } catch (error) {
      container.innerHTML = error;
      throw new Error(error);
    }
  //})
}