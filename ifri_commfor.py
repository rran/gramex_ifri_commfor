import pandas as pd
from gramex.config import variables as cvar
from gramex.cache import open as gxo
import json


def prep_data(data):
    # str2num_replace = {
    #     'Yes': 2,
    #     'No': 1,
    #     'NA': None,
    #     'Increased': 1,
    #     'Decreased': -1,
    #     'Remained same': 0,
    # }
    # prep_df = data.replace(str2num_replace)
    # return prep_df.groupby('cid').sum().reset_index()
    return data.apply(lambda x: x.fillna("N/A"))


def others_df(handler, data):
    odf = prep_data(data)
    if len(handler.args):
        odf.loc[:, 'qfilter'] = 'Others'
        # for acol in handler.args.keys():
        #     odf.loc[:, acol[:-1]] = 'Others'  #  odf[acol].str.replace()
    return odf


def filtered_df(handler, data):
    fdf = prep_data(data)
    if len(handler.args):
        fdf.loc[:, 'qfilter'] = 'matched'
    else:
        fdf.loc[:, 'qfilter'] = 'all'
        # for acol in handler.args.keys():
        #     odf.loc[:, acol[:-1]] = 'Others'  #  odf[acol].str.replace()
    return fdf


def global_filters(data):
    # import pdb;pdb.set_trace();
    return prep_data(data)


# def chart_param(handler):
#     df = gxo(cvar['data_file'])
#     # cparam = handler.args.get('cparam')[0]
#     return json.dumps(df.columns.tolist())


def group_by_rtype(handler, data):
    df = data.groupby(['res_type', 'landscape', 'sid'])
    gdf = df['collect', 'use', 'sell', 'buy'].mean().stack().reset_index()
    gdf = gdf.rename(columns={'level_3': 'metric', 0: 'value'})  #.agg(lambda x: x.mode())
    return filtered_df(handler, gdf)


def others_group_by_rtype(handler, data):
    odf = group_by_rtype(handler, data)
    if len(handler.args):
        odf.loc[:, 'qfilter'] = 'Others'
        # for acol in handler.args.keys():
        #     odf.loc[:, acol[:-1]] = 'Others'  #  odf[acol].str.replace()
    return odf